/*  ============================================================================
     MrDIY Trigger Sensor

     THE SENDER

       Option (1):

                    ESP01
                -------------------------------------
               |       switch value--->| 03/RX   VCC |----(VCC)
               |                       | 00      RST |
               |                       | 02       CH |----(VCC)
               |          collector--->| GND   TX/01 |
                -------------------------------------


       Option (2):
                       ESP12
                      --------------
                     | RST       TX |
                     | ADC       RX |
            (VCC)--->| CH        05 |
                     | 16        04 |
                     | 14        00 |
     switch value--->| 12        02 |
             done--->| 13        15 |<--GND
            (VCC)--->| V        GND |<--Collector
                      --------------

  ============================================================================= */


#include "ESP8266WiFi.h"
#include "espnow.h"

//#define ESP12                   // uncomment if you are using ESP01
//#define DEBUG_FLAG            // uncomment to show the logs
//#define TEST_DEVICE           // uncomment to send random msgs for testing

#define GROUP_ID 6734922

#ifdef ESP12
#define DonePin   13
#define SwitchPin 12
#else
#define SwitchPin 3
#endif

int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;
uint8_t broadcastAddress[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

typedef struct struct_message {
  int group_id;
  uint8_t sensor_id[6];
  int sensor_status;
  float battery_voltage;
} struct_message;

struct_message msg;
ADC_MODE(ADC_VCC);

void setup() {
#ifdef ESP12
  pinMode(DonePin, OUTPUT);
  digitalWrite(DonePin, HIGH);
#endif
  pinMode(SwitchPin, INPUT);

#ifdef DEBUG_FLAG
  Serial.begin(115200);
#endif

  WiFi.mode(WIFI_STA);
  if (esp_now_init() != 0) {
#ifdef DEBUG_FLAG
    Serial.println("Error initializing ESP-NOW, restarting");
#endif
    delay(1000);
    ESP.restart();
  }
  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_register_send_cb(OnDataSent);
  esp_now_add_peer(broadcastAddress, ESP_NOW_ROLE_SLAVE, 1, NULL, 0);
  sendReading();        // send initial state when booted
}

void loop() {

#ifdef TEST_DEVICE
  delay(random(100, 7000));
  sendReading();
#endif
}

void sendReading() {

  msg.group_id = GROUP_ID;
  WiFi.macAddress(msg.sensor_id);
  msg.sensor_status = digitalRead(SwitchPin);
#ifdef TEST_DEVICE
  msg.sensor_status = random(0, 2);
#endif
  msg.battery_voltage = ((float)ESP.getVcc()) / 1024;
#ifdef DEBUG_FLAG
  Serial.println("Button actioned");
#endif
  esp_now_send(broadcastAddress, (uint8_t *) &msg, sizeof(msg));
}


// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {

#ifdef DEBUG_FLAG
  if (sendStatus == 0) {
    char macAddr[18];
    sprintf(macAddr, "%02X%02X%02X%02X%02X%02X", msg.sensor_id[0], msg.sensor_id[1], msg.sensor_id[2], msg.sensor_id[3], msg.sensor_id[4], msg.sensor_id[5]);
    Serial.print("/MrDIYSensor_");
    Serial.print(macAddr);
    Serial.print("  ");
    Serial.print(msg.sensor_status);
    Serial.print(" [");
    Serial.print(msg.battery_voltage);
    Serial.print("v]");
    Serial.println();
  }
  else {
    Serial.println("Delivery fail");
  }
#endif
  gotoSleep();
}

void gotoSleep() {
#ifdef ESP12
  digitalWrite(DonePin, LOW);
#endif
  delay(10);
  ESP.deepSleep(0);      // deep sleep
}
