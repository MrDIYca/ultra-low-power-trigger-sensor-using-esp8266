/*  ============================================================================
     MrDIY Trigger Sensor

     THE RECEIVER

  ============================================================================= */


#include "ESP8266WiFi.h"
#include "espnow.h"
#include "SoftwareSerial.h"


//#define DEBUG_FLAG
#define GROUP_ID 6734922
#define BUZZER_PIN 4              // D2 on Wemos D1 mini, beeps on boot and received msgs

typedef struct struct_message {
  int group_id;
  uint8_t sensor_id[6];
  int sensor_status;
  float battery_voltage;
} struct_message;

// Create a struct_message called myData
struct_message msg;
char end_char = '\n';

SoftwareSerial SSerial(12, 14);       // D6 and D5 on Wemos D1 mini

void setup() {
  SSerial.begin(9600);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
#ifdef DEBUG_FLAG
  Serial.println();
  Serial.print("ESP Board MAC Address:  ");
  Serial.println(WiFi.macAddress());
#endif
  if (esp_now_init() != 0) {
#ifdef DEBUG_FLAG
    Serial.println("Error initializing ESP-NOW, restarting");
#endif
    delay(1000);
    ESP.restart();
  }
  esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
  esp_now_register_recv_cb(OnDataRecv);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  tone(BUZZER_PIN, 1568, 100); delay(200); noTone(BUZZER_PIN); tone(BUZZER_PIN, 1568, 100);
}

void loop() {}


void OnDataRecv(uint8_t *mac_addr, uint8_t *incomingData, uint8_t len) {

  digitalWrite(LED_BUILTIN, LOW);
  SSerial.write(incomingData, len);
  SSerial.write(end_char);

#ifdef DEBUG_FLAG
  memcpy(&msg, incomingData, len);
  if (msg.group_id == GROUP_ID) {
    Serial.print("Received from ");
    char macAddr[18];
    sprintf(macAddr, "%02X%02X%02X%02X%02X%02X", msg.sensor_id[0], msg.sensor_id[1], msg.sensor_id[2], msg.sensor_id[3], msg.sensor_id[4], msg.sensor_id[5]);
    Serial.print(macAddr);
    Serial.print("  ");
    Serial.print(msg.sensor_status);
    Serial.print(" [");
    Serial.print(msg.battery_voltage);
    Serial.print("v]");
    Serial.println();
  } else {
    Serial.println("Wrong Group ID, ignoring");
  }
#endif
  tone(BUZZER_PIN, 1568, 100);
  digitalWrite(LED_BUILTIN, HIGH);

}
