# MrDIY Ultra Low Power Trigger Sensor 

In this project, I will share with you my design and code for battery-based, low-cost, real-time trigger sensors to monitor doors and motion or make them act as wall switches. It uses only microAmps while in standby and has a trigger time of 240ms. Designed using ESP8266, ATtiny and ESPNOW.

## Videos

#### Design Video

[![MrDIY Ultra Low Power Trigger Sensor YouTube video](https://img.youtube.com/vi/HGZFg4SVySM/0.jpg)](https://youtu.be/HGZFg4SVySM)


#### Assembly Video

[![MrDIY Ultra Low Power Trigger Sensor YouTube video](https://img.youtube.com/vi/LbGGeREwECc/0.jpg)](https://youtu.be/LbGGeREwECc)


#### The Hub Video

[![MrDIY Ultra Low Power Trigger Sensor YouTube video](https://img.youtube.com/vi/VyVaxvgBpEU/0.jpg)](https://youtu.be/VyVaxvgBpEU)


## Instructions

Please visit <a href="https://www.instructables.com/Ultra-Low-Power-Trigger-Sensor-Using-ESP8266">my Instructables page</a> page for full instructions or watch the video above.

<p>Don't forget to check out <a href="https://www.youtube.com/channel/UCtfYdcn8F8wfRA2BXp2FPtg">my YouTube channel</a>  and follow me on <a href="https://twitter.com/MrDIYca">Twitter</a>. If you are interested in supporting my work, please visit <a href="https://www.patreon.com/MrDIYca?fan_landing=true">my Patreon page</a>.</p>


## Thanks
Many thanks to all the authors and contributors to the open source libraries and community. You are the reason this project exists.
