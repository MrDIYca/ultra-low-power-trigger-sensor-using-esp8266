/*  ============================================================================
     MrDIY Trigger Sensor

     THE ATTINY

                      ------\/------
            (RESET)  | 1 PB5  VCC 8 |----(VCC)
        esp power<---| 2 PB3  PB2 7 |<---done
     switch value<---| 3 PB4  PB1 6 |<---switch input (INT0/PCINT1)
            (GND)----| 4 GND  PB0 5 |
                     --------------

  ============================================================================= */

//#define DEBUG_FLAG

#include "avr/io.h"
#include "avr/interrupt.h"
#include "avr/sleep.h"

#define SWITCH_INPUT_PIN           PB1                    // Interrupt for Reed Switch
#define ESP_DONE_PIN               PB2                    // INPUT
#define POWER_TO_ESP_PIN           PB3                    // OUTPUT
#define SWITCH_VALUE_PIN           PB4                    // OUTPUT

#define max_time_to_keep_esp8266_powered_on       300     // ms
unsigned long start_time;


/* ################################## Setup ############################################# */

void setup() {

  pinMode(POWER_TO_ESP_PIN, OUTPUT);
  pinMode(SWITCH_VALUE_PIN, OUTPUT);
  pinMode(SWITCH_INPUT_PIN,   INPUT);
  pinMode(ESP_DONE_PIN,  INPUT_PULLUP);
  powerOnEsp();                                         // initi ESP boot
}

/* ################################### Loop ############################################# */

void loop() {
  if ( (millis() - start_time) >= max_time_to_keep_esp8266_powered_on ) nap();
  if ( espIsDone() ) nap();
}


/* ################################### Sleep ########################################### */

void nap() {

  powerOffEsp();

  // InterruptRelated ---------------------------
  MCUCR |= 1 << ISC00;
  MCUCR &= ~(1 << ISC01);             // interrupt at any logical change at INT0
  GIMSK |= 1 << INT0;                 // Enable interrupt  for PB1/PCINT1
  ADCSRA &= ~(1 << ADEN);             // ADC off
  ACSR |= (1 << ACD);                 // Analog comparator off

  // SleepRelated ---------------------------
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // Configure sleep mode (power-down)
  //sleep_bod_disable();
  interrupts();
  sleep_mode();                         // = sleep_enable() +sleep_cpu() +sleep_disable()

  // will resume sleep from here
}

/* ################################### Interrupt ########################################## */

ISR(INT0_vect) {
  powerOnEsp();
}

/* ###################################### Tools ########################################### */

void powerOffEsp() {
  digitalWrite(POWER_TO_ESP_PIN, LOW);         // Turn off the ESP8266
  digitalWrite(SWITCH_VALUE_PIN, HIGH);
}

void powerOnEsp() {
  digitalWrite(POWER_TO_ESP_PIN, LOW);                            // reset ESP8266
  digitalWrite(SWITCH_VALUE_PIN, digitalRead(SWITCH_INPUT_PIN));  // send the switch signal
  digitalWrite(POWER_TO_ESP_PIN, HIGH);                           // Turn on the ESP8266
  start_time = millis();                                          // new start time
}

bool espIsDone() {
  return !digitalRead(ESP_DONE_PIN);    // LOW is true; pulled up
}
