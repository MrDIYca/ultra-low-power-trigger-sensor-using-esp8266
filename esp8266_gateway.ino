/*  ============================================================================
     MrDIY Trigger Sensor

     THE GATEWAY
     
  ============================================================================= */


#include "ESP8266WiFi.h"
#include "PubSubClient.h"
#include "IotWebConf.h"
#include "SoftwareSerial.h"
#include "time.h"
#include "TimeLib.h"

#define DEBUG_FLAG

/* ------ WifiManager -------------------------------------------------------- */
#define thingName  "MrDIY Sensors"
#define wifiInitialApPassword "mrdiy.ca"
char mqttServer[60];
char mqttUserName[32];
char mqttUserPassword[32];
#define  mqttPort  1883

DNSServer             dnsServer;
WebServer             server(80);
IotWebConf            iotWebConf(thingName, &dnsServer, &server, wifiInitialApPassword, "mrd1");
IotWebConfParameter   mqttServerParam = IotWebConfParameter("MQTT server", "mqttServer", mqttServer, sizeof(mqttServer) );
IotWebConfParameter   mqttUserNameParam = IotWebConfParameter("MQTT username", "mqttUser", mqttUserName, sizeof(mqttUserName));
IotWebConfParameter   mqttUserPasswordParam = IotWebConfParameter("MQTT password", "mqttPass", mqttUserPassword, sizeof(mqttUserPassword), "password");
IotWebConfSeparator   separator = IotWebConfSeparator();
WiFiClient            wifiClient;

const char CUSTOMHTML_HEAD[] PROGMEM           = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/><title>Control Panel</title>";
const char CUSTOMHTML_FORM_END[] PROGMEM       = "</fieldset>";
const char CUSTOMHTML_END[] PROGMEM            = "<br /><span style='font-size:10px'><a href='https://www.mrdiy.ca' target='_blank'>MrDIY.ca</a></span></div></body></html>";
const char CUSTOMHTML_UPDATE[] PROGMEM         = "<div style='padding-top:25px;'><a href='{u}'>Firmware update</a></div>";
const char CUSTOMHTML_CONFIG_VER[] PROGMEM     = "<div style='font-size: .6em;'>Firmware config version '{v}'</div>";

class CustomHtmlFormatProvider : public IotWebConfHtmlFormatProvider {

  protected:
    String getHead()      override {
      return String(FPSTR(CUSTOMHTML_HEAD))       + IotWebConfHtmlFormatProvider::getHead();
    }
    String getFormEnd()   override {
      return String(FPSTR(CUSTOMHTML_FORM_END))   + IotWebConfHtmlFormatProvider::getFormEnd();
    }
    String getEnd()       override {
      return String(FPSTR(CUSTOMHTML_END))        + IotWebConfHtmlFormatProvider::getEnd();
    }
    String getUpdate()    override {
      return String(FPSTR(CUSTOMHTML_UPDATE))     + IotWebConfHtmlFormatProvider::getUpdate();
    }
    String getConfigVer() override {
      return String(FPSTR(CUSTOMHTML_CONFIG_VER)) + IotWebConfHtmlFormatProvider::getConfigVer();
    }
};

CustomHtmlFormatProvider customHtmlFormatProvider;

/* --------- SENSORS DATA ------------------------------------------------------ */

#define GROUP_ID 6734922
typedef struct struct_message {
  int group_id;
  uint8_t sensor_id[6];
  int sensor_status;
  float battery_voltage;
} struct_message;

typedef struct sensor_data {
  int group_id;
  uint8_t sensor_id[6];
  int sensor_status;
  float battery_voltage;
  time_t timestamp;
} sensor_data;

#define NUM_SENSORS 20
sensor_data msg;
sensor_data sensors[NUM_SENSORS];
int sensors_saved = 0;
int msg_length = sizeof(struct struct_message);
uint8_t incomingData[sizeof(struct struct_message)];
char end_char = '\n';
int received_msg_length;

/* ---------- MQTT --------------------------------------------------------------- */

#define  MQTT_MSG_SIZE    100
char mqttTopic[sizeof(struct struct_message)];
#define MSG_BUFFER_SIZE  (50)
char mqtt_msg[MSG_BUFFER_SIZE];
PubSubClient mqttClient(wifiClient);

SoftwareSerial SSerial(14, 12);  // D5 and D6 on Wemos D1 mini

/* ############################ setup ############################################# */

void setup() {

  SSerial.begin(9600);
#ifdef DEBUG_FLAG
  Serial.begin(115200);
  Serial.println();
  Serial.println(F("==============================================================="));
  Serial.println(F("  MrDIY Sensors"));
  Serial.println(F("==============================================================="));
  Serial.println();
#endif
  iotWebConf.addParameter(&mqttServerParam);
  iotWebConf.addParameter(&mqttUserNameParam);
  iotWebConf.addParameter(&mqttUserPasswordParam);
  iotWebConf.setWifiConnectionCallback(&wifiConnected);
  iotWebConf.setFormValidator(&formValidator);
  //iotWebConf.setStatusPin(LED_BUILTIN);
  iotWebConf.skipApStartup();
  iotWebConf.setHtmlFormatProvider(&customHtmlFormatProvider);


  boolean validConfig = iotWebConf.init();
  if (!validConfig) {
    mqttServer[0] = '\0';
    mqttUserName[0] = '\0';
    mqttUserPassword[0] = '\0';
  }
  server.on("/", handleRoot);
  server.on("/config", [] { iotWebConf.handleConfig(); });
  server.on("/reboot", [] { rebootDevice(); });
  server.onNotFound([] {  iotWebConf.handleNotFound();  });

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

/* ############################ loop ############################################# */

void loop() {

  iotWebConf.doLoop();
  mqttReconnect();
  mqttClient.loop();

  if (SSerial.available()) {
    received_msg_length = SSerial.readBytesUntil(end_char, incomingData, msg_length );
    if (received_msg_length == msg_length) {
      memcpy(&msg, incomingData, msg_length);
      sensorMessageReceived();
    }
  }
}

void sensorMessageReceived() {

  digitalWrite(LED_BUILTIN, LOW);
  char macAddr[18];
  sprintf(macAddr, "%02X%02X%02X%02X%02X%02X", msg.sensor_id[0], msg.sensor_id[1], msg.sensor_id[2], msg.sensor_id[3], msg.sensor_id[4], msg.sensor_id[5]);
  String payload = "{\"data\":{";
  payload += "\"state\":" + String(msg.sensor_status) + ",";
  payload += "\"battery\":" + String(msg.battery_voltage, 2);
  payload += "}}";
  saveSensorData();
  mqttPublish(macAddr, "status", payload );
#ifdef DEBUG_FLAG
  Serial.print(" ");
  Serial.println(payload.c_str());
#endif

#ifdef DEBUG_FLAG
  if ( false && msg.group_id == GROUP_ID) {
    Serial.print("MQTT/MrDIYSensor_");
    Serial.print(macAddr);
    Serial.print("  ");
    Serial.print(msg.sensor_status);
    Serial.print(" [");
    Serial.print(msg.battery_voltage);
    Serial.print("v]");
    Serial.println();
  } else {
    // Serial.println("Wrong Group ID, ignoring");
  }
#endif
  digitalWrite(LED_BUILTIN, HIGH);
}

void saveSensorData() {

  bool found = false;
  int i;
  for (i = 0; i < sensors_saved; i++) {
    if ( sensors[i].sensor_id[4] == msg.sensor_id[4] && sensors[i].sensor_id[5] == msg.sensor_id[5]) {
      sensors[i].sensor_status = msg.sensor_status;
      sensors[i].battery_voltage = msg.battery_voltage;
      sensors[i].timestamp = time(nullptr);
      found = true;
    }
  }
  if (!found) {
    for (i = 0; i < 6; i++) sensors[sensors_saved].sensor_id[i] = msg.sensor_id[i];
    sensors[sensors_saved].sensor_status = msg.sensor_status;
    sensors[sensors_saved].battery_voltage = msg.battery_voltage;
    sensors[sensors_saved].timestamp = time(nullptr);
    sensors_saved++;
  }
}
/* ############################ WifiManager ############################################# */

void wifiConnected() {

  mqttReconnect();
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
  digitalWrite(LED_BUILTIN, HIGH);
#ifdef DEBUG_FLAG
  Serial.print("wifiConnected: ");
#endif
}

void configSaved() {

  // does nothing for now
}

boolean formValidator() {

  boolean valid = true;
  int l = server.arg(mqttServerParam.getId()).length();
  if (l == 0) {
    mqttServerParam.errorMessage = "Please provide the MQTT server address (x.x.x.x)";
    valid = false;
  }
  return valid;
}

void handleRoot() {

  if (iotWebConf.handleCaptivePortal()) return;

  String s = "<!DOCTYPE html><html lang='en'><head><meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'/>";
  s += "<title>MrDIY Sensors</title>";
  s += "<meta http-equiv='refresh' content='10'>";
  s += "<style>body{text-align: center;font-family:verdana;} button{border:0;border-radius:0.3rem;background-color:#16A1E7;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%;} fieldset{border-radius:0.3rem;margin: 0px;}label{float:left;}.w5{min-width:50%;}.w3{min-width:30%;text-align:right}.w1{min-width:10%;text-align:right}.row{margin:5px;font-size:0.8em;color:#444444;padding-bottom:0px;}</style>";
  s += "</head><body>";
  if (sensors_saved) s += "<div style='text-align:left;display:inline-block;min-width:600px;'><h1>Sensors (" + String(sensors_saved) + ")</h1><fieldset>";
  else              s += "<div style='text-align:left;display:inline-block;min-width:600px;'><h1>No Sensors detected</h1>";

  for (int i = 0; i < sensors_saved; i++) {
    char macAddr[18];
    sprintf(macAddr, "%02X%02X%02X%02X%02X%02X", sensors[i].sensor_id[0], sensors[i].sensor_id[1], sensors[i].sensor_id[2], sensors[i].sensor_id[3], sensors[i].sensor_id[4], sensors[i].sensor_id[5]);
    s += "<div class='row'>";
    s += "<label class='w5'>stat/mrdiy_sensor_" + String(macAddr) + "/status</label>";
    s += "<label class='w1'><strong>" + String(sensors[i].sensor_status) + "</strong></label>";
    s += "<label class='w1'>" + String(sensors[i].battery_voltage) + "v</label>";
    s += "<label class='w3'>" + getFormatedDuration(time(nullptr) - sensors[i].timestamp) + "</label>";
    s += "</div>";
  }
  //s += "<div class='row'><label class='w6'>/MrDIYSensor_FE3412AC89EE</label><label class='w2'>1</label><label class='w2'>3.9v</label><label class='w2'>3.9v</label></div>";
  s += "</fieldset><br />";
  s += "<a href='config'><button type='submit'>Config</button></a><br /><br />";
  s += "<a href='reboot'><button type='submit'>Reboot</button></a>";
  s += "<span style='font-size:10px'><a href='https://www.mrdiy.ca' target='_blank'>MrDIY.ca</a></span></div>";
  s += "</body></html>\n";

  server.send(200, "text/html; charset=UTF-8", s);
}


void rebootDevice() {
  ESP.restart();
}
/* ############################ MQTT ############################################# */

void mqttReconnect() {

  if (!mqttClient.connected()) {
    mqttClient.setServer( mqttServer, mqttPort);
    mqttClient.setCallback(onMqttMessage);
    if (mqttClient.connect("MrDIY Sensors", mqttUserName, mqttUserPassword)) {
      mqttClient.subscribe("cmnd/mrdiy_sensor");
      mqttClient.publish("stat/mrdiy_sensor/status", "online");
#ifdef DEBUG_FLAG
      Serial.println(F("stat/mrdiy_sensor/status  online"));
      Serial.println();
      Serial.println(F("Connected to MQTT"));
      Serial.println(F("Gateway ready ......"));
#endif
    }
  }
}

void mqttPublish(char macAdress[], char topic[], String payload) {

  strcpy (mqttTopic, "stat/mrdiy_sensor_");
  strcat (mqttTopic, macAdress);
  strcat (mqttTopic, "/");
  strcat (mqttTopic, topic);
#ifdef DEBUG_FLAG
  Serial.print(mqttTopic);
#endif
  mqttClient.publish(mqttTopic, payload.c_str());
}


void onMqttMessage(char* topic, byte* payload, unsigned int length)  {

  // currently not listening to anything

}


/* ############################ EXTRA ############################################ */


String getFormatedDuration(time_t duration) {

  String str;
  if (duration >= SECS_PER_DAY) {
    str = (duration / SECS_PER_DAY);
    str += ("d ");
    duration = duration % SECS_PER_DAY;
  }
  if (duration >= SECS_PER_HOUR) {
    str += (duration / SECS_PER_HOUR);
    str += ("h ");
    duration = duration % SECS_PER_HOUR;
  }
  if (duration >= SECS_PER_MIN) {
    str += (duration / SECS_PER_MIN);
    str += ("m ");
    duration = duration % SECS_PER_MIN;
  }
  str += (duration);
  str += ("s");
  return str;
}
